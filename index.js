const express = require('express');
const app = express();
const mysql = require('mysql');
const bodyParser = require('body-parser');
const md5 = require('js-md5');
const session = require('express-session');
const FileStore = require('session-file-store')(session);


const session_options = {
  path: "sessions",
  useAsync: true,
  reapInterval: 5000,
  maxAge: 10000
};


app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
  store: new FileStore(session_options),
  secret: 'secretword',
  resave: true,
  saveUninitialized: true
}));


exports.connection = mysql.createConnection({
  host     : '127.0.0.1',
  user     : 'root',
  password : '',
  database : 'angular',
  multipleStatements: true
});


exports.md5 = md5;
exports.session = session;


const controllers = {
    site: require(__dirname + '/controllers/site'),
    admin: require(__dirname + '/controllers/admin')
};


app.use('/data', (req, res, next) => {
    switch(req.path){
        case '/indexitems': return controllers.site.indexitems(req, res); break;
        case '/getcats': return controllers.site.getcats(req, res); break;
        case '/getitem': return controllers.site.getitem(req, res); break;
        case '/catitems': return controllers.site.catitems(req, res); break;
        case '/search': return controllers.site.search(req, res); break;
        case '/cartitems': return controllers.site.cartitems(req, res); break;
        case '/order': return controllers.site.order(req, res); break;
        default: '';
    }
});


app.post('/checklogin', (req, res, next) => {
    return controllers.admin.login(req, res); next();
});


const isAdmin = (req, res, next) => {
    if (!session['login']){
        res.json({admin: false});
    }
    else {
        next();
    }
};


app.use('/admin', isAdmin, function(req, res, next){
    switch(req.path){
        case '/': return controllers.admin.orders(req, res); break;
        case '/orders/': return controllers.admin.orders(req, res); break;
        case '/items/': return controllers.admin.items(req, res); break;
        case '/addcat/': return controllers.admin.addcat(req, res); break;
        case '/editcat/': return controllers.admin.editcat(req, res); break;
        case '/additem/': return controllers.admin.additem(req, res); break;
        case '/edititem/': return controllers.admin.edititem(req, res); break;
        case '/order/': return controllers.admin.order(req, res); break;
        default: '';
    }
});


app.get('*', (req, res) => {
    res.sendFile(__dirname + '/public/index.html');
});


app.listen(3000);