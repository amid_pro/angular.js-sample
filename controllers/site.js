const app = require('../index');


exports.getcats = (req, res) => {
    app.connection.query('select id, name from cats order by name asc', (error, results) => {
        if (results) {
            res.json(results);
        }
        if (error) {
            res.json({error: error});
        }
    });
};


exports.indexitems = (req, res) => {
    app.connection.query('select id, name, price, image from items limit ?', [20], (error, results) => {
        if (results) {
            res.json(results);
        }
        if (error) {
            res.json({error: error});
        }
    });
};


exports.catitems = (req, res) => {
    
    const limit = 12;
    const cat_id = req.query.id;
    const page = req.query.page;
    let rows = 0;
    
    if (page > 0) {
        rows = (req.query.page - 1) * limit;
    }
    
    app.connection.query('select name from cats where id = ? limit 1; select id from items where cat_id = ?; select id, name, price, image from items where cat_id = ? limit ?, ?', [cat_id, cat_id, cat_id, rows, limit], (error, results) => {
        if (results[1].length < 1) {
            res.json({error: 'Нет товаров в этой категории'});
        } else {
            res.json({itemscount: results[1].length, limit: limit, items: results[2], cat: results[0]});
        }
    });
};


exports.getitem = (req, res) => {
    app.connection.query('select items.id, items.name, items.cat_id, items.price, items.image, items.description, cats.name as cat_name from items, cats where items.id = ? and cats.id = items.cat_id limit 1', [req.query.id], (error, results) => {
        if (results) {
            res.json(results);
        }
        if (error) {
            res.json({error: error});
        }
    });
};


exports.search = (req, res) => {
    app.connection.query('select id, name, price, image from items where name like ? or description like ? limit 24', ['%' + req.query.s + '%', '%' + req.query.s + '%'], (error, results) => {
        if (results) {
            res.json({items: results});
        }
        if (error) {
            res.json({error: error});
        }
    });
};


exports.cartitems = (req, res) => {
    app.connection.query('select id, name, price, image from items where id IN (?)', [JSON.parse(req.query.ids)], (error, results) => {
        if (results) {
            res.json({items: results});
        }
        if (error) {
            res.json({error: error});
        }
    });
};


exports.order = (req, res) => {

    const name = req.body.name;
    const email = req.body.email;
    const items = req.body.items;

    app.connection.beginTransaction((err) => {
        if (err) {
            throw err;
        }

        app.connection.query('insert into orders set order_time = ?, name = ?, email = ?', [new Date().getTime(), name, email], (err, order_result) => {
            if (err) {
                return app.connection.rollback(() => {
                    throw err;
                });
            }

            const order_id = order_result.insertId;

            app.connection.query('select id, price from items where id in (?)', [Object.keys(items)], (error, results) => {
                let sum = 0;
                for (let i = 0; i < results.length; i++) {
                    sum += results[i].price * items[results[i].id]; 

                    app.connection.query('insert into order_items set order_id = ?, item_id = ?, item_count = ?, item_price = ?; update orders set order_sum = order_sum + ? where id = ? limit 1', [order_id, results[i].id, items[results[i].id], results[i].price, sum, order_id], (error, results) => {
                        if (error) {
                            return app.connection.rollback(() => {
                                throw err;
                            });
                        }
                    });

                }
            });

            app.connection.commit((err) => {
                if (err) {
                    return app.connection.rollback(() => {
                        throw err;
                    });
                }
            });

            res.json({order_id: order_id});
            
        });
    });
};