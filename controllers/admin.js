const app = require('../index');


exports.login = (req, res) => {
    const login = req.body.login;
    const password = req.body.password;
  
    app.connection.query('select id from admin where login = ? and password = ? limit 1', [login, app.md5(password)], (error, results) => {
        if (results){
            if (results.length === 1){
                app.session['login'] = results[0].id;
                res.json({login: results[0].id});
            }
            else {
                res.json({error: 'Неправильный логин или пароль'});
            }
        }
        if (error){
            res.json({error: error});
        }
    });
};


exports.items = (req, res) => {
    const cat = req.body.cat_id > 0 ? (' = ' + app.connection.escape(req.body.cat_id)) : (' > 0 ');
    app.connection.query('select id, name, price, description, image from items where cat_id ' + cat, (error, results) => {
        if (results){
            res.json({items: results});
        }
    });
};


exports.addcat = (req, res) => {
    const new_cat = req.body.new_cat;
    app.connection.query('insert into cats set name = ?', [new_cat], (error, results) => {
        if (results){
            res.json({id: results.insertId});
        }
    });
};


exports.editcat = (req, res) => {
    app.connection.query('update cats set name = ? where id = ? limit 1', [req.body.cat.name, req.body.cat.id], (error, results) => {
        if (results){
            res.json({rows: results.affectedRows});
        }
    });
};


exports.additem = (req, res) => {
    app.connection.query('insert into items set name = ?, cat_id = ?, price = ?, description = ?, image = ?', [req.body.item.name, req.body.item.cat_id, req.body.item.price, req.body.item.description, req.body.item.image], (error, results) => {
        if (results){
            res.json({id: results.insertId});
        }
    });
};


exports.edititem = (req, res) => {
    app.connection.query('update items set name = ?, cat_id = ?, price = ?, description = ?, image = ? where id = ? limit 1', [req.body.item.name, req.body.item.cat_id, req.body.item.price, req.body.item.description, req.body.item.image, req.body.item.id], (error, results) => {
        if (results){
            res.json({rows: results.affectedRows});
        }
    });
};


exports.orders = (req, res) => {
    
    const limit = 12;
    const page = req.query.page;
    let rows = 0;
    
    if (page > 0){
        rows = (req.query.page - 1) * limit;
    }    
    
    app.connection.query('select id from orders; select * from orders order by id desc limit ?, ?', [rows, limit], (error, results) => {
        if (results){
            res.json({limit: limit, order_count: results[0].length, orders: results[1]});
        }
    });
};


exports.order = (req, res) => {
    var order_id = req.query.order_id;
    app.connection.query('select * from orders where id = ?; select order_items.item_id, order_items.item_count, order_items.item_price, items.name from order_items, items where order_items.order_id = ? and order_items.item_id = items.id', [order_id, order_id], (error, results) => {
        if (results){
            res.json({order: results[0], items: results[1]});
        }
    });
};
