(() => {
    
    'use strict';
    
    const changeTitle = (title) => {
        document.title = title;
    };
    
    
    const app = angular.module("app", ['ngRoute']);
    
    
    app.run(($rootScope, $sce) => {
        $rootScope.settings = {
            sitename: 'NodeShop',
            date: new Date().getFullYear()
        };
        
        
        $rootScope.trustAsHtml = (string) => {
            return $sce.trustAsHtml(string);
        };
        
        
        const getCartCount = () => {
            let c = 0;
            Object.keys($rootScope['cartitems']).map((k, v) => {
                c += $rootScope['cartitems'][k];
            });
            return c;
        };
        
        
        if (!localStorage['cartitems']) {
            $rootScope['cartitems'] = {};
            $rootScope['cartcount'] = 0;
        }
        else {
            $rootScope['cartitems'] = JSON.parse(localStorage['cartitems']);
            $rootScope['cartcount'] = getCartCount();
        }
        
        
        $rootScope.addToCart = (id) => {
            
            if ($rootScope['cartitems'][id]){
                $rootScope['cartitems'][id]++;
            }
            else {
                $rootScope['cartitems'][id] = 1;
            }
            
            localStorage['cartitems'] = JSON.stringify($rootScope['cartitems']);
            $rootScope['cartcount']++;
        };
        
        
        $rootScope.removeCartItem = (id) => {
            $rootScope['cartcount'] -= $rootScope['cartitems'][id];
            delete $rootScope['cartitems'][id];
            localStorage['cartitems'] = JSON.stringify($rootScope['cartitems']);
        };
        
        
        $rootScope.updateCartItem = (id, item_count) => {
            $rootScope['cartitems'][id] = parseInt(item_count);
            $rootScope['cartcount'] = getCartCount();
            localStorage['cartitems'] = JSON.stringify($rootScope['cartitems']);
        };
        
        
        $rootScope.clearCart = () => {
            localStorage.clear();
            $rootScope['cartitems'] = [];
            $rootScope['cartcount'] = 0;
        };
        
    });
    
    
    app.config(['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) => {
        const admTpl = '/tpl/admin/admin.html';    
        $locationProvider.html5Mode(true);    
        $routeProvider.
                when('/', {templateUrl: '/tpl/index.html',  controller: indexCtrl}).
                when('/category/:id/:page?/', {templateUrl: '/tpl/category.html',  controller: categoryCtrl}).
                when('/item/:id/', {templateUrl: '/tpl/item.html', controller: itemCtrl}).
                when('/search/', {templateUrl: '/tpl/search.html', controller: searchCtrl}).
                when('/login/', {templateUrl: '/tpl/login.html', controller: loginCtrl}).
                when('/cart/', {templateUrl: '/tpl/cart.html', controller: cartCtrl}).
                when('/adm/', {templateUrl: admTpl, controller: admCtrl, url: 'index'}).
                when('/adm/order/:id/', {templateUrl: admTpl, controller: admCtrl, url: 'order'}).
                when('/adm/items/', {templateUrl: admTpl, controller: admCtrl, url: 'items'}).
                when('/adm/cats/', {templateUrl: admTpl, controller: admCtrl, url: 'cats'}).
                when('/adm/add_item/', {templateUrl: admTpl, controller: admCtrl, url: 'add_item'}).
                when('/adm/item/:id/', {templateUrl: admTpl, controller: admCtrl, url: 'edit_item'}).
                when('/404/', {template: '<h1>Страница не найдена</h1>'}).
                otherwise({redirectTo: '/404/'}); 
    }]); 


    app.factory('getData', ($http) => {
        const data = (url, query) => {
            return $http.get(url);
         };
        return {
          data: data
        };
    });    

    
    app.factory('postData', ($http) => {
        const data = (url, params) => {
            return $http.post(url, params);
         };
        return {
          data: data
        };
    });    
    
    
    app.controller('leftMenuCtrl', ($scope, getData) => {
        getData.data('/data/getcats').then((data) => {
            $scope.cats = data.data;
        });   
    });
    
    
    function categoryCtrl($scope, $routeParams, getData){
        $scope.id = $routeParams.id;
        getData.data('/data/catitems?id=' + $routeParams.id + "&page=" + ($routeParams.page || 0)).then((data) => {

            if (data.data.error){
                $scope.error = data.data.error;
            }
            else {
                $scope.cat = data.data.cat[0];
                $scope.items = data.data.items;
                $scope.itemscount = data.data.itemscount;
                $scope.limit = data.data.limit;
                changeTitle($scope.cat.name);

                if ($scope.itemscount > $scope.limit){
                    let arr = [];
                    for (let i = 0; i < (Math.ceil($scope.itemscount / $scope.limit)); i++){
                        arr[i] = i + 1;
                    }
                    $scope.pagination = {
                        currentpage: $routeParams.page || 1,
                        pages: arr

                    };
                }
            }
            
        });
    }
    
    
    function itemCtrl($scope, $routeParams, $location, getData){
        getData.data('/data/getitem?id=' + $routeParams.id).then((data) => {
            $scope.item = data.data[0];
            if (!$scope.item){
                $scope.error = 'Товар не найден';
                changeTitle($scope.error);
            }
            else {
                changeTitle($scope.item.name);
            }
        });
    }
    
    
    function indexCtrl($scope, $rootScope, getData){
        getData.data('/data/indexitems').then((data) => {
            $scope.items = data.data;
        });        
        changeTitle($rootScope.settings.sitename);
    }
    
    
    function searchCtrl($scope, $rootScope, $routeParams, getData){
        getData.data('/data/search?s=' + $routeParams.s).then((data) => {
            $scope.items = data.data.items;
            
            if ($scope.items.length < 1){
                $scope.error = 'По запросу "' + $routeParams.s + '" не найдено ни одного товара';
            }

        });
        changeTitle('Поиск');
    }


    function cartCtrl($scope, $rootScope, getData, postData){
        
        const ids = Object.keys($rootScope['cartitems']);
        
        $scope.error = 'Нет товаров в корзине';
        if (ids.length > 0){
            getData.data('/data/cartitems?ids=' + JSON.stringify(ids)).then((data) => {
                $scope.items = data.data.items;
                if ($scope.items){
                    $scope.error = ''; 
                }
                
                $scope.getSumm = () => {
                    $scope.summ = 0;
                    for (let i = 0; i < $scope.items.length; i++){
                         $scope.summ += $rootScope['cartitems'][$scope.items[i]['id']] * $scope.items[i]['price'];
                    }
                    return $scope.summ;
                };
                
                $scope.getSumm();

                $scope.sendOrder = () => {
                    if ($scope.user.name && $scope.user.email && $rootScope['cartitems']){
                        postData.data('/data/order', {name: $scope.user.name, email: $scope.user.email, items: $rootScope['cartitems']}).then((data) => {
                            if (data.data.order_id){
                                delete $scope.summ;
                                delete $scope.items;
                                $scope.user.name = '';
                                $scope.user.email = '';
                                $rootScope.clearCart();
                                $scope.error = 'Заказ оформлен. номер заказа: ' + data.data.order_id;
                            }
                        });
                    }
                };
                
            });
            
        }
        changeTitle('Корзина');
    }


    function loginCtrl($scope, $location, postData){
        $scope.submit = () => {
            postData.data('/checklogin', $scope.user).then((data) => {
                if (data.data.error){
                    $scope.message = data.data.error;
                }
                if (data.data.login){
                    $location.path('/adm');
                }
            });
        };       
        changeTitle('Авторизация');
    }


    function admCtrl($scope, $route, $location, $routeParams, getData, postData){
        
        getData.data('/admin').then((data) => {
            if (data.data.admin === 'false'){
                $location.path('/login/');
            }
            
            switch($route.current.$$route.url){
                
                case 'index':
                    (() => {
                        $scope.tpl = 'index.html';
                        changeTitle($scope.title = 'Заказы');
                        
                        const page = $routeParams.page || 0;
                        getData.data('/admin/orders/?page=' + page).then((data) => {
                            $scope.orders = data.data.orders;
                            $scope.order_count = data.data.order_count;
                            $scope.limit = data.data.limit;

                            if ($scope.order_count > $scope.limit){
                                let arr = [];
                                for (let i = 0; i < (Math.ceil($scope.order_count / $scope.limit)); i++){
                                    arr[i] = i + 1;
                                }
                                $scope.pagination = {
                                    currentpage: $routeParams.page || 1,
                                    pages: arr
                                };
                            }
                            
                        });
                        
                    })(); break;
                
                case 'order':
                    (() => {
                        $scope.tpl = 'order.html';
                        changeTitle($scope.title = 'Заказ № ' + $routeParams.id);
                        getData.data('/admin/order/?order_id=' + $routeParams.id).then((data) => {
                            $scope.order = data.data.order[0];
                            $scope.items = data.data.items;
                        });
                    })(); break;
                    
                    
                case 'items': 
                    (() => {
                        $scope.tpl = 'items.html';
                        changeTitle($scope.title = 'Товары');
                        getData.data('/data/getcats').then((data) => {
                            $scope.cats = data.data;
                        });
                        
                        $scope.cat_id = '';
                        
                        const getitems = (cat_id) => {
                            postData.data('/admin/items/', {cat_id: cat_id}).then((data) => {
                                $scope.items = data.data.items;
                            });                            
                        };
                        
                        getitems();
                        
                        $scope.setcat = (id) => {
                            $scope.cat_id = id;
                            getitems(id);
                        };
                        
                    })(); break;
                    
                case 'cats': 
                    (() => {
                        $scope.tpl = 'cats.html';
                        changeTitle($scope.title = 'Категории');
                        getData.data('/data/getcats').then((data) => {
                            $scope.cats = data.data;
                        });
                        
                        $scope.checkName = (name) => {
                            for (let i = 0; i < $scope.cats.length; i++){
                                if ( $scope.cats[i].name === name ) { return $scope.cats[i].id; }
                            }
                            return false;
                        };
                        
                        $scope.add_cat = (new_cat) => {
                            if ($scope.checkName(new_cat) !== false){
                                alert('Такая категория уже существует');
                            }
                            else {
                                postData.data('/admin/addcat/', {new_cat: new_cat}).then((data) => {
                                    if (data.data.id){
                                        $scope.cats.push({id: data.data.id, name: new_cat});
                                    }
                                });
                            }
                        };
                        
                        $scope.edit_cat = (cat) => {
                            const check = $scope.checkName(cat.name);
                            if (check === false || check === cat.id){
                                 postData.data('/admin/editcat/', {cat: cat}).then((data) => {
                                    console.log(data);
                                });
                            }
                            else {
                                alert('Такая категория уже существует');
                            }

                        };
                        
                    })(); break;
                    
                case 'edit_item': 
                    (() => {
                        changeTitle($scope.title = 'Редактирование товара');
                        $scope.tpl = 'add_item.html';
                        getData.data('/data/getcats').then((data) => {
                            $scope.cats = data.data;     
                        });
                        getData.data('/data/getitem?id=' + $routeParams.id).then((data) => {
                            $scope.item = data.data[0];
                        });
                        
                        $scope.edititem = (item) => {
                            postData.data('/admin/edititem/', {item: item}).then((data) => {
                                if (data.data.rows === 1){
                                    alert('Товар отредактирован');
                                }
                            });
                        };                        
                    })(); break;
                    
                case 'add_item': 
                    (() => {
                        changeTitle($scope.title = 'Добавление товара');
                        $scope.tpl = 'add_item.html';
                        getData.data('/data/getcats').then((data) => {
                            $scope.cats = data.data;
                        });
                      
                        $scope.additem = (item) => {
                            postData.data('/admin/additem/', {item: item}).then((data) => {
                                if (data.data.id){
                                    alert('Товар добавлен');
                                    $route.reload();
                                }
                            });
                        };
                        
                    })(); break;
                    
                default:'';                   
            }
            
            if ($scope.tpl){
                $scope.path_tpl = '/tpl/admin/' + $scope.tpl;
            }
            
        });
    }

})();